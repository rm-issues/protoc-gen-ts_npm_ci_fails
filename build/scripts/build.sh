#!/bin/bash

BASE_DIR=$(dirname "$0")
ROOT_DIR=$BASE_DIR/../..
cd $BASE_DIR/../..

rm -rf $ROOT_DIR/lib

# $BASEDIR/build.combine_proto.sh
npm run build:protoc
npm run build:tsc