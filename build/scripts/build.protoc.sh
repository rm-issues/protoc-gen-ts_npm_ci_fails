#!/bin/bash

BASEDIR=$(dirname "$0")
cd ${BASEDIR}/../..

PROTO_DEST=./lib

PROTO_DEST_TS=${PROTO_DEST}/ts

mkdir -p ${PROTO_DEST}
mkdir -p ${PROTO_DEST_TS}


# Code generation TS
node $(npm bin)/grpc_tools_node_protoc \
    --plugin=protoc-gen-ts=${BASEDIR}/../../node_modules/.bin/protoc-gen-ts \
    --ts_out=${PROTO_DEST_TS} \
    -I ./src \
    src/**/*.proto
