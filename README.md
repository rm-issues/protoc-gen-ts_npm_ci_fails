# Issue protoc-gen-ts_npm_ci_fails

## Steps to reproduce

The build is working as expected when using npm install

```
npm install
npm run build
```

In this case we get a lib folder containing a js and a ts folder. The js folder is irrelevant for this issue. However if we install using npm ci we see the ts folder contains .ts files which can be converted to javascript files


However if we use npm ci the outcome is different (don't forget to delete node_modules before trying npm ci). So if we do the same thing with

```
npm ci
npm run build
```

We will now get a ts folder again but this time it will only contain typescript defintition files (*.d.ts).


### Behaviour as is

```
npm install
npm run build 
```

results in correct typescript files

```
npm ci
npm run build
```

results in declarations only

### Expceted Behaviour

both ways should behave the same and output correct typescript files